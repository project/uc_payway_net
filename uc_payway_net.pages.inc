<?php

/**
 * @file
 * PayWay Net ubercart module pages.
 */

/**
 * Notification callback.
 *
 * Read post from PayWay system and create reply
 * starting with: 'cmd=_notify-validate'...
 * then repeating all values sent: that's our VALIDATION.
 */
function uc_payway_net_payment_notify() {
  global $user;
  // @TODO: This foreach() does ... nothing at all?
  foreach ($_POST as $key => $value) {
    if (get_magic_quotes_gpc()) {
      // Fix issue with magic quotes.
      $value = stripslashes($value);
    }

    if (!preg_match("/^[_0-9a-z-]{1,30}$/i", $key) || !strcasecmp($key, 'cmd')) {
      unset($key);
      unset($value);
    }
  }

  $param_string = "";
  foreach ($_POST as $name => $value) {
    $params[$name] = $value;
    if ($name != "username" && $name != "password") {
      $param_string .= "$name=$value;";
    }
  }
  foreach ($_GET as $name => $value) {
    $params[$name] = $value;
    if ($name != "username" && $name != "password") {
      $param_string .= "$name=$value;";
    }
  }

  // Our 'validation' is that the user/pass are correct.
  if ($params['username'] != variable_get('uc_payway_net_security_username', '')
    || $params['password'] != variable_get('uc_payway_net_security_password', '')) {
    // Usually this means you haven't configured your
    // security username and security password correctly in
    // Ubercart PayWay Net module configuration.
    //
    // If your settings are exactly right, someone's trying
    // to send a fraudulent payment notification.
    watchdog('uc_payway_net', 'Denied access to PayWay Net notification endpoint.', [], WATCHDOG_ERROR);
    drupal_access_denied();
  }
  else {
    // Unsure why PayWay Net returns the amount with a thousands separator,
    // but there you have it.
    $params['am_payment'] = str_replace(',', '', $params['am_payment']);
    $order_id = trim($params['payment_reference']);
    if ($order = uc_order_load($order_id)) {
      if ($params['fl_success'] == '1') {
        $comment = t('Payway Net payment_reference: @payment_reference', [
          '@payment_reference' => $params['payment_reference'],
        ]);
        $targs = [
          '@amount' => $params['am_payment'],
          '@order_id' => $order_id,
          '!order_link' => l(t('order #@order_id', [
            '@order_id' => $order_id,
          ]), 'admin/store/orders/' . $order_id),
        ];
        watchdog('uc_payway_net', 'Payment of $@amount reported by PayWay Net for !order_link', $targs, WATCHDOG_INFO);
        uc_order_comment_save($order_id, $user->uid, t('Payment of @amount submitted via PayWay Net.', $targs), 'admin', 'payment_received  ');
        uc_order_comment_save($order_id, $user->uid, t('PayWay Net reported a payment of @amount.', $targs), 'order', 'payment_received');
        uc_payment_enter($order_id, 'payway_net', $params['am_payment'], $order->uid, [], $comment);
        uc_cart_complete_sale($order);
      }
      else {
        watchdog('uc_payway_net', 'Payment failure reported by PayWay Net for order @order_id', ['@order_id' => $order_id], WATCHDOG_ERROR);
        uc_order_comment_save($order_id, $user->uid, t("Attempted payment failed for order @payment_reference.", ['@payment_reference' => $order_id]), 'order');
      }
      // Tell PayWay that everything's fine.
      return "Success";
    }
    else {
      // If error results, return HTTP 500.
      watchdog('uc_payway_net', 'Received notification from PayWay Net, but unable to load order with ID %oid.', ['!oid' => $params['payment_reference']], WATCHDOG_ERROR);
      drupal_add_http_header('Status', '503 Unable to locate order for update');
      die("Error: Unable to locate order for update.");
    }
  }
}

/**
 * Redirect to PayWay.
 */
function uc_payway_net_payment_redirect() {
  module_load_include('inc', 'uc_payway_net');
  $biller_code = variable_get('uc_payway_net_biller_code', '');
  $merchant_id = variable_get('uc_payway_net_merchant_id', 'TEST');
  $paypal_email = variable_get('uc_payway_net_paypal_email', '');
  $username = variable_get('uc_payway_net_security_username', '');
  $password = variable_get('uc_payway_net_security_password', '');
  $base_url = variable_get('uc_payway_net_payway_url', '');
  $order_id = intval($_SESSION['cart_order']);
  $order = uc_order_load($order_id);
  $token_variables = [
    'username' => $username,
    'password' => $password,
    'biller_code' => $biller_code,
    'merchant_id' => $merchant_id,
    'paypal_email' => $paypal_email,
    'payment_reference' => $order_id,
    'payment_amount' => $order->order_total,
    'return_link_url' => url('cart/uc_payway_net/complete/' . $order_id, ['absolute' => TRUE]),
    'return_link_url_pre_payment' => url('cart/checkout', ['absolute' => TRUE]),
  ];
  $token = _uc_payway_net_get_token($token_variables);
  $url = $base_url . 'MakePayment?biller_code=' . $biller_code . '&token=' . $token;
  uc_order_update_status($order_id, 'pending');
  drupal_goto($url, [], 302);
}

/**
 * Complete payment page.
 */
function uc_payway_net_payment_complete($order = NULL) {
  module_load_include('inc', 'uc_payway_net');
  $validate = TRUE;

  if (is_null($order)) {
    watchdog('uc_payway_net', 'Unable to locate order.', [], WATCHDOG_ERROR);
    return drupal_access_denied();
  }

  if ($order->payment_method != 'payway_net') {
    watchdog('uc_payway_net', 'PayWay redirect failed payment method check.', [], WATCHDOG_ERROR);
    $validate = FALSE;
  }

  if (!isset($_SESSION['cart_order']) || intval($_SESSION['cart_order']) != $order->order_id) {
    watchdog('uc_payway_net', 'PayWay redirect failed session check.', [], WATCHDOG_ERROR);
    $validate = FALSE;
  }

  $encrytion_key = variable_get('uc_payway_net_encryption_key', '');
  $signature = $_GET['Signature'];
  $params = $_GET['EncryptedParameters'];
  if (!$params = _uc_payway_net_decrypt_parameters($encrytion_key, $params, $signature)) {
    watchdog('uc_payway_net', 'PayWay redirect failed hash check.', [], WATCHDOG_ERROR);
    $validate = FALSE;
  }
  else {
    $targs = [
      '!summary' => $params['payment_status'],
      '@code' => $params['response_code'],
      '@response' => $params['response_text'],
      '!order' => l(t('order #@order_id', [
        '@order_id' => $order->order_id,
      ]), 'admin/store/orders/' . $order->order_id),
    ];

    // Hash validated but transaction failed.
    if ($params['summary_code'] != 0) {
      $validate = FALSE;
      watchdog('uc_payway_net', 'PayWay redirect reported transaction for !order !summary, code @code: @response.', $targs, WATCHDOG_NOTICE);
      drupal_set_message(t('Sorry, your transaction was not completed. The payment gateway reported: @response.', $targs), 'error');
      drupal_goto('cart');
    }
  }

  if ($validate) {
    watchdog('uc_payway_net', 'PayWay redirect reported transaction for !order !summary, code @code: @response.', $targs, WATCHDOG_NOTICE);
    $_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_complete'] = TRUE;
    drupal_goto('cart/checkout/complete');
  }
  else {
    // PayWay Net notification will have to sort the order out if it is valid.
    // Give the user an ambiguous message in case they aren't actually bad.
    drupal_set_message(t('Thank you for your order! PayWay will notify us once your payment has been processed.'));
    drupal_goto('cart');
  }
}
