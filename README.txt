# PayWay Net Payment Module #

## Requirements: ##

  - Webserver with SSL Enabled
  - PHP 7.0+
  - PHP libcurl module installed
  - Copy of cacerts.crt supplied by PayWay team

## Setup PayWay: ##

Below we specify only the required settings to configure PayWay for use with this module. 
As you set up PayWay, feel free to configure parameters not specified below as desired.

  - Login to PayWay and choose Setup from the Net Setup menu
  - Choose Bill Payments/Shopping Cart and click next
  - Biller Code:
    - Take note of your biller code
    - Click Next
  - Notifications:
    - Merchant Notification Email Address: Your email address
    - Allow Customer Receipt Email: Ticked
    - Browser Return:
      - Browser Return URL: https://yoursite.com/
      - Browser Return Text: Back to <YourSite>
    - Server-To-Server Payment Notification:
      - URL: (Must be over SSL) https://yoursite.com/uc_payway_net/payment/notify
      - Email: Your email
      - Payment Status: all
      - Post Type: extended
    - Click Next
  - Security Information
    - Security Tokens: Take note of your Username and Password
    - IP Address: Enter the IP address of your web server (or proxy) and click add
    - Click Next
  - If you wish to configure your PayWay payment pages further, continue to click Next

To install PayWay Module in Ubercart:

  - Copy the uc_payway_net folder into your sites/all/modules directory.
  - Place cacerts.crt from PayWay team in module directory.
  - Log into your Ubercart admin area
  - In Administer->Site Building->Modules, enable PayWay Net
  - In Administer->Store administration->Configuration->Payment Settings, enable PayWay Net
  - Click on PayWay Net Settings
  - Enter all the details you took note of when setting up PayWay
  - If your webserver requires a proxy, expand the proxy settings and configure accordingly.
  - Click save

Congratulations, you're done! You can now accept payments through PayWay!