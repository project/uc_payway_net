<?php

/**
 * @file
 * PayWay Net ubercart module additions.
 */

/**
 * Callback to check if certificate exists.
 */
function _uc_payway_net_cert_exists() {
  $cert_path = drupal_get_path('module', 'uc_payway_net') . '/cacerts.crt';
  return file_exists($cert_path) ? $cert_path : FALSE;
}

/**
 * Get PayWay Net token.
 */
function _uc_payway_net_get_token($parameters) {
  $proxy_host_setting = variable_get('uc_payway_net_proxy_host', '');
  $proxy_port_setting = variable_get('uc_payway_net_proxy_port', '');
  $proxy_user_setting = variable_get('uc_payway_net_proxy_username', '');
  $proxy_pass_setting = variable_get('uc_payway_net_proxy_password', '');
  $proxy_host = strlen($proxy_host_setting) == 0 ? NULL : $proxy_host_setting;
  $proxy_port = strlen($proxy_port_setting) == 0 ? NULL : $proxy_port_setting;
  $proxy_user = strlen($proxy_user_setting) == 0 ? NULL : $proxy_user_setting;
  $proxy_pass = strlen($proxy_pass_setting) == 0 ? NULL : $proxy_pass_setting;
  $cert_file = drupal_get_path('module', 'uc_payway_net') . '/cacerts.crt';
  $payway_base_url = variable_get('uc_payway_net_payway_url', '');

  // Find the port setting, if any.
  // @TODO: parse_url().
  $payway_url = $payway_base_url;
  $port = 443;
  $port_pos = strpos($payway_base_url, ":", 6);
  $url_end_pos = strpos($payway_base_url, "/", 8);
  if ($port_pos !== FALSE && $port_pos < $url_end_pos) {
    $port = (int) substr($payway_base_url, ((int) $port_pos) + 1, ((int) $url_end_pos));
    $payway_url = substr($payway_base_url, 0, ((int) $port_pos)) . substr($payway_base_url, ((int) $url_end_pos), strlen($payway_base_url));
  }

  $ch = curl_init($payway_url . "RequestToken");

  if ($port != 443) {
    curl_setopt($ch, CURLOPT_PORT, $port);
  }

  curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
  curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
  curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  // Set proxy information as required.
  if (!is_null($proxy_host) && !is_null($proxy_port)) {
    curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
    curl_setopt($ch, CURLOPT_PROXY, $proxy_host . ":" . $proxy_port);
    if (!is_null($proxy_user)) {
      if (is_null($proxy_pass)) {
        curl_setopt($ch, CURLOPT_PROXY_USERPWD, $proxy_user . ":");
      }
      else {
        curl_setopt($ch, CURLOPT_PROXY_USERPWD, $proxy_user . ":" . $proxy_pass);
      }
    }
  }

  // Set timeout options.
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);

  // Set references to certificate files.
  curl_setopt($ch, CURLOPT_CAINFO, $cert_file);

  // Check the existence of a common name in the SSL peer's certificate
  // and also verify that it matches the hostname provided.
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);

  // Verify the certificate of the SSL peer.
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

  // Build the parameters string to pass to PayWay.
  $parameters_string = '';
  $init = TRUE;
  foreach ($parameters as $name => $value) {
    if ($init) {
      $init = FALSE;
    }
    else {
      $parameters_string .= '&';
    }
    $parameters_string .= urlencode($name) . '=' . urlencode($value);
  }

  curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters_string);

  // Make the request.
  $text = curl_exec($ch);

  // Check the response for errors.
  $errnum = curl_errno($ch);
  $errmsg = curl_error($ch);
  $headermsg = 'HTTP/1.1 403 ' . $errmsg;

  if ($errnum != 0) {
    watchdog('uc_payway_net', 'cURL error getting token, Error %errnum, %errmsg', ['%errnum' => $errnum, '%errmsg' => $errmsg], WATCHDOG_ERROR);
    header($headermsg);
    exit;
  }

  curl_close($ch);

  // Split the response into parameters.
  $rsp_parameter_array = explode("&", $text);
  $rsp_parameters = [];
  foreach ($rsp_parameter_array as $parameter) {
    list($name, $value) = explode("=", $parameter, 2);
    $rsp_parameters[$name] = $value;
  }

  if (array_key_exists('error', $rsp_parameters)) {
    watchdog('uc_payway_net', 'Error getting token: %err', ['%err' => $rsp_parameters['error']], WATCHDOG_ERROR);
    header($headermsg);
    exit;
  }
  else {
    return $rsp_parameters['token'];
  }
}

/**
 * Callback to decrypt parameters.
 */
function _uc_payway_net_decrypt_parameters($encrytion_key, $encrypted_text, $signature) {
  $key = base64_decode($encrytion_key);
  $iv = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
  // OpenSSL alternative for MCrypt rijndael-128 cbc.
  $method = 'aes-128-ecb';

  $text = openssl_decrypt(base64_decode($encrypted_text), $method, $key, OPENSSL_RAW_DATA, $iv);
  $text = _uc_payway_net_pkcs5_unpad($text);

  $hash = openssl_decrypt(base64_decode($signature), $method, $key, OPENSSL_RAW_DATA, $iv);
  $hash = bin2hex(_uc_payway_net_pkcs5_unpad($hash));

  // Compute the MD5 hash of the parameters.
  $text_hash = md5($text);
  // Check the provided MD5 hash against the computed one.
  if ($text_hash != $hash) {
    trigger_error("Invalid parameters signature");
  }
  $params = [];
  // Loop through each parameter provided.
  foreach (explode("&", $text) as $parameter) {
    list($name, $value) = explode("=", $parameter);
    $params[urldecode($name)] = urldecode($value);
  }
  return $params;
}

/**
 * Callback to modify key.
 */
function _uc_payway_net_pkcs5_unpad($text) {
  $pad = ord($text[strlen($text) - 1]);
  if ($pad > strlen($text)) {
    return FALSE;
  }
  if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) {
    return FALSE;
  }
  return substr($text, 0, -1 * $pad);
}

/*
function _uc_payway_response_code($code) {
$codes = array(
'00' => 'Approved or completed successfully',
'01' => 'Refer to card issuer',
'03' => 'Invalid merchant',
'04' => 'Pick-up card',
'05' => 'Do not honour',
'08' => 'Honour with identification',
'12' => 'Invalid transaction',
'13' => 'Invalid amount',
'14' => 'Invalid card number (no such number)',
'30' => 'Format error',
'36' => 'Restricted card',
'41' => 'Lost card',
'42' => 'No universal account',
'43' => 'Stolen card, pick up',
'51' => 'Not sufficient funds',
'54' => 'Expired card',
'61' => 'Exceeds withdrawal amount limits',
'62' => 'Restricted card',
'65' => 'Exceeds withdrawal frequency limit',
'91' => 'Issuer or switch is inoperative',
'92' => 'Financial institution or intermediate network facility
cannot be found for routing',
'94' => 'Duplicate transmission',
'Q2' => 'Transaction Pending',
'Q3' => 'Payment Gateway Connection Error',
'Q4' => 'Payment Gateway Unavailable',
'QD' => 'Invalid Payment Amount - Payment amount less than minimum/exceeds
maximum allowed limit',
'QE' => 'Internal Error',
'QI' => 'Transaction incomplete - contact Westpac to confirm reconciliation',
'QQ' => 'Invalid Credit Card or Verification Number',
'QX' => 'Network Error has occurred',
'QY' => 'Card Type Not Accepted',
'QZ' => 'Zero value transaction',
);
return $codes[$code];
}

function _uc_payway_summary_code($code) {
$codes = array(
0 => 'success',
1 => 'failure',
2 => 'error',
3 => 'rejected',
);
return $codes[$code];
}
 */
